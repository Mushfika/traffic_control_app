import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:traffic_control_app/provider/authentication_provider.dart';
import 'package:traffic_control_app/provider/rfid_provider.dart';
import 'package:traffic_control_app/provider/user_info_provider.dart';
import 'package:traffic_control_app/screens/screen_signup.dart';

import 'navigation/app_router.dart';
import 'navigation/navigation_service.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<UserInfoProvider>(
          create: (context) => UserInfoProvider(),
        ),
        ChangeNotifierProvider<AuthenticationProvider>(
          create: (context) => AuthenticationProvider(),
        ),
        ChangeNotifierProvider<RfidProvider>(
          create: (context) => RfidProvider(),
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: NavigationService.navigatorKey,
      debugShowCheckedModeBanner: false,
      home: ScreenSignUp(),
      onGenerateRoute: AppRouter.generateRoute,
    );
  }
}
