import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:traffic_control_app/provider/authentication_provider.dart';
import 'package:traffic_control_app/screens/screen_signup.dart';

class ScreenLogin extends StatefulWidget {
  const ScreenLogin({Key? key}) : super(key: key);

  @override
  State<ScreenLogin> createState() => _ScreenLoginState();
}

class _ScreenLoginState extends State<ScreenLogin> {
  TextEditingController? emailController = TextEditingController();
  TextEditingController? passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Consumer<AuthenticationProvider>(builder: (_, authProvider, ___) {
        return SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Text(
                    "Hello, Welcome back to your account",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.grey[700],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Column(
                  children: [
                    makeInput(
                        label: "Email",
                        onChangedValue: (value) {},
                        obscureText: false,
                        controller: emailController),
                    makeInput(
                      label: "Password",
                      onChangedValue: (value) {},
                      obscureText: true,
                      controller: passwordController,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.teal,
                    borderRadius: BorderRadius.circular(50),
                    border: const Border(
                      bottom: BorderSide(color: Colors.black),
                      top: BorderSide(color: Colors.black),
                      left: BorderSide(color: Colors.black),
                      right: BorderSide(color: Colors.black),
                    ),
                  ),
                  child: MaterialButton(
                    minWidth: double.infinity,
                    height: 50,
                    onPressed: () {
                      print("email: ${emailController?.value.text}");
                      print("password: ${passwordController?.value.text}");
                      Provider.of<AuthenticationProvider>(context,
                              listen: false)
                          .doSignIn({
                        'email': emailController?.value.text,
                        'password': passwordController?.value.text,
                      });

                      // if (Provider.of<AuthenticationProvider>(context,
                      //             listen: false)
                      //         .status ==
                      //     'failed') {
                      //   ScaffoldMessenger.of(context).showSnackBar(
                      //       const SnackBar(content: Text('Login failed')));
                      // }
                    },
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: Text(
                      "LogIn",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Don't have an account?"),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return ScreenSignUp();
                      }));
                    },
                    child: Text(
                      "SignUp",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: Colors.blue,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget makeInput({label, obscureText, controller, onChangedValue}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 5,
        ),
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 5,
                offset: const Offset(0, 2),
              ),
            ],
            borderRadius: BorderRadius.circular(50),
          ),
          child: TextField(
            onChanged: onChangedValue,
            controller: controller,
            obscureText: obscureText,
            decoration: InputDecoration(
                labelText: label,
                labelStyle: TextStyle(color: Colors.grey.shade500),
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                fillColor: Colors.white,
                filled: true,
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(color: Colors.grey.shade400)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(color: Colors.grey.shade400))),
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
