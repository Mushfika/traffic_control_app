import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:traffic_control_app/provider/authentication_provider.dart';
import 'package:traffic_control_app/screens/screen_user.dart';
import 'package:traffic_control_app/screens/screen_login.dart';

class ScreenSignUp extends StatefulWidget {
  const ScreenSignUp({super.key});

  @override
  State<ScreenSignUp> createState() => _ScreenSignUpState();
}

class _ScreenSignUpState extends State<ScreenSignUp> {
  TextEditingController? emailController = TextEditingController();
  TextEditingController? passwordController = TextEditingController();
  TextEditingController? nameController = TextEditingController();

  // @override
  // void didChangeDependencies() async {
  //   super.didChangeDependencies();
  //   AuthenticationProvider authProvider = Provider.of<AuthenticationProvider>(context, listen:false);
  //   await authProvider.doRegister();
  // }
  @override
  void dispose() {
    emailController?.dispose();
    passwordController?.dispose();
    nameController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<AuthenticationProvider>(builder: (_, authProvider, ___) {
        return SafeArea(
          child: Container(
            //padding: EdgeInsets.symmetric(horizontal: 40),
            width: double.infinity,
            height: MediaQuery.of(context).size.height - 90,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Text(
                      "Create an account",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey[700],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: [
                      makeInput(
                          label: "Email",
                          obscureText: false,
                          onChangedValue: (value) {
                            print(value);
                          },
                          controller: emailController),
                      makeInput(
                        label: "Password",
                        obscureText: true,
                        onChangedValue: (value) {
                          print(value);
                        },
                        controller: passwordController,
                      ),
                      makeInput(
                          label: "Name",
                          obscureText: false,
                          onChangedValue: (value) {
                            print(value);
                          },
                          controller: nameController),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.teal,
                      borderRadius: BorderRadius.circular(50),
                      border: const Border(
                        bottom: BorderSide(color: Colors.black),
                        top: BorderSide(color: Colors.black),
                        left: BorderSide(color: Colors.black),
                        right: BorderSide(color: Colors.black),
                      ),
                    ),
                    child: MaterialButton(
                      minWidth: double.infinity,
                      height: 50,
                      onPressed: () {
                        print("email: ${emailController?.value.text}");
                        print("password: ${passwordController?.value.text}");
                        print("name: ${nameController?.value.text}");
                        Provider.of<AuthenticationProvider>(context,
                                listen: false)
                            .doRegister({
                          'email': emailController?.value.text,
                          'password': passwordController?.value.text,
                          'name': nameController?.value.text
                        });
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ScreenUser();
                        }));
                      },
                      // color: AppColors.primary,
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Text(
                        "SIGNUP",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Already have an account?"),
                    InkWell(
                      onTap: () async {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ScreenLogin();
                        }));
                      },
                      child: Text(
                        " LogIn",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.blue,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget makeInput({label, obscureText, controller, onChangedValue}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 5,
                offset: const Offset(0, 2),
              ),
            ],
            borderRadius: BorderRadius.circular(50),
          ),
          child: TextField(
            controller: controller,
            obscureText: obscureText,
            onChanged: onChangedValue,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                fillColor: Colors.white,
                filled: true,
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(color: Colors.grey.shade400)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(color: Colors.grey.shade400))),
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
