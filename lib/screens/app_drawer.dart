import 'package:flutter/material.dart';
import 'package:traffic_control_app/navigation/navigation_service.dart';
import 'package:traffic_control_app/screens/widgets/drawer_item.dart';

import '../navigation/route_paths.dart';

class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: 300,
      child: Scaffold(
        backgroundColor: Colors.teal,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 80.0, left: 28),
                child: Container(),
              ),
              SizedBox(
                height: 100,
              ),
              Drawer_Item(
                drawerText: "Home".toUpperCase(),
                drawerIcon: Icons.home,
                onPressed: () {
                  // NavigationService().navigateReplaced(RoutePaths.admin);
                },
              ),
              SizedBox(
                height: 30,
              ),
              Drawer_Item(
                drawerText: "Log Out".toUpperCase(),
                drawerIcon: Icons.arrow_back_ios,
                onPressed: () {
                  NavigationService().navigateReplaced(RoutePaths.registration);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
