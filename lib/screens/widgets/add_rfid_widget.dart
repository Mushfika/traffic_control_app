import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:traffic_control_app/provider/rfid_provider.dart';

import '../../navigation/navigation_service.dart';

class AddRfidWidget extends StatefulWidget {
  const AddRfidWidget({
    super.key,
  });

  @override
  State<AddRfidWidget> createState() => _AddRfidWidgetState();
}

class _AddRfidWidgetState extends State<AddRfidWidget> {
  // String? deviceName;
  //
  // int? deviceId;
  // String? hubSerialId;
  TextEditingController rfidController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return _pageBody(context);
  }

  _pageBody(BuildContext context) {
    return Consumer<RfidProvider>(builder: (_, provider, ___) {
      return Padding(
        padding: EdgeInsets.all(16),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          InkWell(
            onTap: () {
              NavigationService().goBack();
            },
            child: Align(
              alignment: Alignment.topRight,
              child: Icon(
                Icons.clear,
                size: 24,
              ),
            ),
          ),
          SizedBox(height: 16),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              children: [
                makeInput(
                    label: "RFID number",
                    onChangedValue: (value) {},
                    obscureText: false,
                    controller: rfidController),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.teal,
                borderRadius: BorderRadius.circular(50),
                border: const Border(
                  bottom: BorderSide(color: Colors.black),
                  top: BorderSide(color: Colors.black),
                  left: BorderSide(color: Colors.black),
                  right: BorderSide(color: Colors.black),
                ),
              ),
              child: MaterialButton(
                minWidth: double.infinity,
                height: 50,
                onPressed: () {
                  print("rfid: ${rfidController.value.text}");
                  Provider.of<RfidProvider>(context, listen: false).createRfid({
                    'rfid': rfidController.value.text,
                  });
                },
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Text(
                  "Submit",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ]),
      );
    });
  }

  Widget makeInput({label, obscureText, controller, onChangedValue}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 5,
        ),
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 5,
                offset: const Offset(0, 2),
              ),
            ],
            borderRadius: BorderRadius.circular(50),
          ),
          child: TextField(
            onChanged: onChangedValue,
            controller: controller,
            obscureText: obscureText,
            decoration: InputDecoration(
                labelText: label,
                labelStyle: TextStyle(color: Colors.grey.shade500),
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                fillColor: Colors.white,
                filled: true,
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(color: Colors.grey.shade400)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(color: Colors.grey.shade400))),
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
