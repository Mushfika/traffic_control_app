import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:traffic_control_app/provider/user_info_provider.dart';

import 'app_drawer.dart';

class ScreenUser extends StatefulWidget {
  @override
  State<ScreenUser> createState() => _ScreenUserState();
}

class _ScreenUserState extends State<ScreenUser> {
  @override
  void initState() {
    super.initState();
    fetchUserInfo();
  }

  Future<void> fetchUserInfo() async {
    UserInfoProvider userInfoProvider =
        Provider.of<UserInfoProvider>(context, listen: false);
    await userInfoProvider.getUserList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text('User'),
      ),
      drawer: DrawerPage(),
      body: Consumer<UserInfoProvider>(
        builder: (_, provider, __) {
          if (provider.status == "loading") {
            return const Center(child: CircularProgressIndicator());
          } else if (provider.status == "failed") {
            return const Center(child: Text("Failed to load user info"));
          } else if (provider.userInfo != null) {
            return ListView.separated(
              itemCount: provider.userInfo?.length ?? 0,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 150,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 24.0, left: 8.0),
                              child: Text(
                                "Name: ${provider.userInfo?[index].name ?? 'Unknown'}",
                                style: const TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, left: 8.0),
                              child: Text(
                                "RFID: ${provider.userInfo?[index].rfid ?? 'Unknown'}",
                                style: const TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, left: 8.0),
                              child: Text(
                                "Date: ${provider.userInfo?[index].date ?? '0'}",
                                style: const TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, left: 8.0),
                              child: Text(
                                "Time: ${provider.userInfo?[index].time ?? '0'}",
                                style: const TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return SizedBox(height: 5);
              },
            );
          } else {
            return Center(child: Text("No user info available"));
          }
        },
      ),
    );
  }
}
