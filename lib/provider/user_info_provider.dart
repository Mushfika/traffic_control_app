import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import '../models/user_info.dart';
import '../repository/user_info_repository.dart';

class UserInfoProvider with ChangeNotifier {
  List<UserInfo>? userInfo;
  String? _status = "loading";

  String get status => _status!;

  set status(String value) {
    _status = value;
    notifyListeners();
  }

  // UserInfo? get userInfo => _userInfo;

  Future<String> getUserList() async {
    try {
      Response response = await UserInfoRepository().getUserInfo();
      print(
          "-----------------------------------------------------------------${response.data}");
      print(
          "-----------------------------------------------------------------${response.statusCode}");
      if (response.statusCode == 200) {
        List<dynamic> dataList = response.data;
        if (dataList.isNotEmpty) {
          //userInfo = UserInfo.fromJson(dataList[1]);
          userInfo = dataList.map((data) => UserInfo.fromJson(data)).toList();
          print("Parsed userInfo list: $userInfo");
          status = "success"; // Print userInfo email
        } else {
          status = "no_data";
          print("No data found in the response.");
        }
        //   userInfo = UserInfo.fromJson(response.data);
        //   print("+++++++++++++++++++++++++++++++++++++++${userInfo}");
        //   print(userInfo?.email);
        //   status = "success";
        notifyListeners();
      } else {
        status = "failed";
        notifyListeners();
      }
    } catch (e) {
      print("Error occurred: $e");
      status = "failed";
      notifyListeners();
    } finally {
      notifyListeners();
    }
    return status;
  }
}
