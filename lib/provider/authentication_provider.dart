import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import '../common_widgets/custom_toast.dart';
import '../models/user_info.dart';
import '../repository/authentication_repository.dart';
import '../repository/user_info_repository.dart';

class AuthenticationProvider with ChangeNotifier {
  String? _status = "loading";

  String get status => _status!;

  set status(String value) {
    _status = value;
    notifyListeners();
  }

  Future<void> doRegister(Map<String, dynamic> data) async {
    status = "loading";
    notifyListeners();
    try {
      Response response = await AuthenticationRepository().doRegistration(data);
      print(response);
      print(response.statusCode);
      print(response.data);
      print(response.statusMessage);
      if (response.statusCode == 200) {
        status = "success";
      }
      notifyListeners();
      showCustomToast(
        msg: 'Successfully Registered',
      );
    } catch (e) {
      status = "failed";

      notifyListeners();
    }
  }

  Future<void> doSignIn(Map<String, dynamic> data) async {
    status = "loading";
    notifyListeners();
    try {
      Response response = await AuthenticationRepository().doLogin(data);
      print(response);
      print(response.statusCode);
      print(response.data);
      print(response.statusMessage);
      if (response.statusCode == 200) {
        status = "success";
      }
      notifyListeners();
      showCustomToast(
        msg: 'Successfully Login',
      );
    } catch (e) {
      status = "failed";
      notifyListeners();
    }
  }
}
