import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:traffic_control_app/navigation/navigation_service.dart';
import '../common_widgets/custom_toast.dart';
import '../models/user_info.dart';
import '../repository/authentication_repository.dart';
import '../repository/rfid_repository.dart';
import '../repository/user_info_repository.dart';

class RfidProvider with ChangeNotifier {
  String? _status = "loading";

  String get status => _status!;

  set status(String value) {
    _status = value;
    notifyListeners();
  }

  Future<void> createRfid(Map<String, dynamic> data) async {
    status = "loading";
    notifyListeners();
    try {
      Response response = await RfidRepository().addRfid(data);

      print(response);
      print(response.statusCode);
      print(response.data);
      print(response.statusMessage);
      if (response.statusCode == 200) {
        status = "success";
        NavigationService().goBack();
      }
      notifyListeners();
      showCustomToast(
        msg: 'RFID added',
      );
    } catch (e) {
      status = "failed";
      notifyListeners();
    }
  }
}
