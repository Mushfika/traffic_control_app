import 'dart:io';

import 'package:flutter/material.dart';
import 'package:traffic_control_app/navigation/route_paths.dart';
import 'package:traffic_control_app/screens/screen_admin.dart';
import 'package:traffic_control_app/screens/screen_signup.dart';

import '../screens/screen_user.dart';

class AppRouter {
  static Route generateRoute(RouteSettings settings) {
    Widget widget;

    switch (settings.name) {
      // case RoutePaths.splash:
      //   widget = Container();
      //   break;
      // case RoutePaths.numberInput:
      //   widget = NumberInputScreen(isRegistration: settings.arguments as bool?);
      //   break;
      case RoutePaths.user:
        widget = ScreenUser();
        break;
      case RoutePaths.admin:
        widget = ScreenAdmin();
        break;
      case RoutePaths.registration:
        widget = ScreenSignUp();
        break;

      default:
        return MaterialPageRoute(
          builder: (context) => Container(),
        );
    }

    if (Platform.isIOS) {
      return MaterialPageRoute(
        builder: (context) {
          return WillPopScope(
            onWillPop: () => _onPop(context),
            child: widget,
          );
        },
        settings:
            RouteSettings(name: settings.name, arguments: settings.arguments),
      );
    }

    return _createRoute(settings, widget);
  }

  static Route _createRoute(final RouteSettings settings, final widget) {
    return PageRouteBuilder(
      settings: settings,
      pageBuilder: (_, animation, secondaryAnimation) => widget,
      transitionsBuilder: (_, animation, secondaryAnimation, child) {
        var begin = const Offset(1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Future<bool> _onPop(BuildContext context) async {
    if (Navigator.of(context).userGestureInProgress) {
      return Future<bool>.value(false);
    }
    return Future<bool>.value(true);
  }
}
