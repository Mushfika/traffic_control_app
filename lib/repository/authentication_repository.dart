import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../navigation/navigation_service.dart';
import '../navigation/route_paths.dart';

const Duration duration = Duration(seconds: 5);

class AuthenticationRepository {
  Future<Response> doRegistration(Map<String, dynamic> data) async {
    Response response = await Dio()
        .post("http://54.199.157.33/auth/register", data: data)
        .timeout(duration);
    print(response);
    return response;
  }

  Future<Response> doLogin(Map<String, dynamic> data) async {
    Response response = await Dio()
        .post("http://54.199.157.33/auth/login", data: data)
        .timeout(duration);
    if (response.statusCode == 200) {
      // Save tokens and user type to SharedPreferences
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('access_token', response.data['access_token']);
      prefs.setString('refresh_token', response.data['refresh_token']);
      prefs.setString('user_type', response.data['user_type']);

      if (response.data['user_type'] == 'user') {
        NavigationService().navigateTo(RoutePaths.user);
      } else {
        NavigationService().navigateTo(RoutePaths.admin);
      }
    }
    print(response);
    return response;
  }
}
