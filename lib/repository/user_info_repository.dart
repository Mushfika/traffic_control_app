import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

const Duration duration = Duration(seconds: 5);

class UserInfoRepository {
  Future<Response> getUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? accessToken = prefs.getString('access_token');
    print('Access Token before API call: $accessToken');
    // if (accessToken == null) {
    //   throw Exception("Access token not found");
    // }
    //print(accessToken);
    Response response = await Dio()
        .get(
          "http://54.199.157.33/rfid/all",
          options: Options(
            headers: {
              'Authorization': 'Bearer $accessToken',
            },
          ),
        )
        .timeout(duration);
    return response;
  }
}
