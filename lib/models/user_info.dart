class UserInfo {
  int? id;
  String? rfid;
  bool? status;
  String? date;
  String? time;
  String? name;

  UserInfo({this.id, this.rfid, this.status, this.date, this.time, this.name});

  UserInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    rfid = json['rfid'];
    status = json['status'];
    date = json['date'];
    time = json['time'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['rfid'] = this.rfid;
    data['status'] = this.status;
    data['date'] = this.date;
    data['time'] = this.time;
    data['name'] = this.name;
    return data;
  }
}
