import 'package:flutter/material.dart';

Widget customFloatingActionButton(
    Function() onTap, String buttonText, TextStyle textStyle) {
  return InkWell(
    onTap: onTap,
    child: Container(
      height: 48,
      decoration: BoxDecoration(
        color: Colors.teal,
        // borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        //mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            buttonText,
            style: textStyle,
          ),
        ],
      ),
    ),
  );
}
