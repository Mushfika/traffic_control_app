import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showCustomToast(
    {required String msg, Color? bg, ToastGravity? gravity, Toast? length}) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: length ?? Toast.LENGTH_SHORT,
      gravity: gravity ?? ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: bg ?? Colors.grey,
      textColor: Colors.white,
      fontSize: 16.0);
}
