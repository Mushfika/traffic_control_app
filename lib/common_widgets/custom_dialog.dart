import 'package:flutter/material.dart';

import '../navigation/navigation_service.dart';

Future<dynamic> showCustomDialog(
    {required Widget dialogChild,
    BoxDecoration? dialogDecoration,
    bool? isDismissible,
    double? height}) async {
  var result = await showGeneralDialog(
    context: NavigationService().getContext()!,
    barrierLabel: "Barrier",
    barrierDismissible: isDismissible ?? false,
    barrierColor: Colors.grey.withOpacity(0.1),
    transitionDuration: const Duration(milliseconds: 250),
    pageBuilder: (_, __, ___) {
      return Dialog(
          insetPadding: EdgeInsets.all(24),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(14)),
              side: BorderSide(color: Colors.white24)),
          child: dialogChild);
    },
    transitionBuilder: (_, anim, __, child) {
      Tween<Offset> tween;
      if (anim.status == AnimationStatus.reverse) {
        tween = Tween(begin: const Offset(0, 1), end: Offset.zero);
      } else {
        tween = Tween(begin: const Offset(0, -1), end: Offset.zero);
      }

      return SlideTransition(
        position: tween.animate(anim),
        child: FadeTransition(
          opacity: anim,
          child: child,
        ),
      );
    },
  );

  return result;
}
